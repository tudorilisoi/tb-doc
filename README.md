# This is Teambutler documentation on bitbucket

--- Work in progress ---



#Teambutler development environment

##An important note for developers

Please ask your questions here by creating issues (press **Create issue**)

<https://bitbucket.org/tudorilisoi/tb-doc/issues?status=new&status=open>

Please **ask one question per issue** (make issues atomic). 

This will enable us to better focus on each issue without diverging offtopic

Once the answer is added to this document, the corresponding bitbucket issue will be closed.

Also, please read existing issues before adding new ones, this will avoid duplicates.

When applicable, use chapter numbers from this document to disambiguate the subject of your question/issue


##Introduction

Teambutler is in essence a web page, and there is a web server which makes it accessible through the internet

Teambutler is in fact a set of servers running behind an nginx reverse proxy. (more on that later)

## 1. The frontend 

...is mainly written in Javascript (let's call it JS from now on for the sake of brevity). 

The code is mainly based on a custom framework called Kopi

Other libraries:

 - jQuery (surprised?)
 - knockout.js (A React.js-style view renderer) <http://knockoutjs.com/>
 - moment.js <https://momentjs.com/>
 - ...and all the other bower dependencies
 

Package managent is done with bower. That was all the hype in 2012-2013 :)

There are 2 ways to run Teambutler in the browser: a debug mode which uses raw scripts (no bundling, no minification), and a bundle mode, respectively 

The module loader is require.js http://requirejs.org/

## 1.1. The build process

This project uses grunt.js as a task runner/build tool. Gulp.js is also used for development

The JS is bundled using r.js optimizer <http://requirejs.org/docs/optimization.html>

**Note: the build process only deals with JS and LessCSS/CSS.** 

It merely bundles and minifies JS for faster download in the browser and to avoid exposing the JS source code to the public internet.

There is a build.sh in the root folder, which merely runs some grunt tasks and r.js

The build process will prepare the app for deployment on a production server.

It does:

 - .less to .css compilation (grunt `less` task)
 - bundle and uglify js source files for faster download (using `r.js`) 
  

**How to build the frontend:** 
You simply run it (form an *nix environment, like vagrant) by typing **./built.sh** in the root folder of the project. 

Result:
Building (re-)creates a `teambutler/built` folder, which is suitable for production use

## 1.2 Deploying the frontend build on production

This can be done by copying the `teambutler/built` folder on your webserver or by running `./build.sh` directly on the webserver


## 2. The backend

The backend consists of:
 
  - a) an **Yii framework(version 1.\*)/PHP** based web application <http://www.yiiframework.com/doc/api/>.

The Yii app is  located at `teambutler/protected`

This project uses **Composer** <https://getcomposer.org/> as a PHP package/dependency manager, see `composer.json` in the root folder.

  - b) **a websocket server for realtime data updates** (uses node.js), located in `teambutler/messageserver/socketio_server.js`
  - c) **a (forked) etherpad-lite node.js server** for realtime collaborative editing on Teambulter topics.
  Its project folder is `teambutler/etherpad/etherpad-lite`
  
  Etherpad fork is hosted here <https://github.com/tudorilisoi/etherpad-lite>
 
**There is no build process for the backend**, you just `git checkout` you desired branch on the production server

### 2.1 How does it all fit together?

Although there are 3 servers involved (PHP, etherpad-lite and websockets), you access teambutler on a single base URL.

How is this done? Nginx acts as a reverse proxy and routes `/api/*` to PHP/Yii (2.a), `/etherpad/*` to etherpad-lite (2.c) and /livesync to the websocket server (2.b), respectively.

## 4. Development process

INTRODUCTORY NOTE: JS tests only cover a small part of the application. 
They do not even pass. 
You may curse all you want, this is the harsh truth.

This is a long-running project, so it has accumulated quite a bit of technical debt and various style inconsistencies 
in the code.

## 4.1. Introduction

For historical reasons, there are 2 APIs for data access.
For that matter, there are in fact two applications with overlapping functionality implemended differently

  - a) **Kopi** based app at `teambutler/client/`
 
  - b) **ProgramSchedule** (we'll call it **PS** from now on), which, by the way, does more than program scheduling, located in the subfolder `teambutler/client/program_schedule`.
  This is based heavily on knockout.js

Both APIs are working and may be used in production.

Should you encounter problems, please [create an issue](https://bitbucket.org/tudorilisoi/tb-doc/issues?status=new&status=open) with your specific question

### 4.1.1 Kopi.Model

Kopi has a Model concept. 

A Model instance maps to a database record and can be retrieved/updated/deleted using Kopi libraries 

`teambutler/client/components/kopi/lib/Model.js` 

which interrogates a PHP REST endpoint 

`teambutler/protected/controllers/TBRESTApiController.php`

Another good starting points for understanding the Kopi REST API are 

`teambutler/client/app/dataloader/showDefinitionLoader.js` 

and a specific model inherited for Kopi.Model 

`teambutler/client/app/models/ShowDefinitionModel.js`

**Advantages**

 - uses classic REST, so you know what to expect. 

**Disadvantages**

 - may create duplicate Javascript `Model` instances of the same database record

### 4.1.2 The storage2 library

I invented GraphQL before Facebook did. 

Joking aside, **storage2** or shortly **s2**, is a JS library 
designed to mirror parts (you may call them subtrees/subgraphs, since the database is relational)
of the MySQL database into the browser Javascript environment.

It is lacking both tests and documentation, but hopefully you will be able to learn by example.

s2 somewhat resembles Facebook's **GraphQL** and **meteor.js** (live updates and fetching arbitrary trees of related records)

When fetching with s2, you get a record and its  related records and their respecive related records and so on

Think of a book library:
Fetching a book may also fetch `book->comments`, `book->author`, `book->author->author_address`, 
if this is what you want. 

s2 will do all that in a single HTTP request, as opposed to REST.


There is a concrete commented example below.

#### Key concepts

A `DataModelYii` instance (called model) is a JS representation of a database record. 

As stated before, a model may have links to related models 

"Model" and "Related models" are Yii concepts, check <http://www.yiiframework.com/doc/guide/1.1/en/database.arr> and 

`teambutler/protected/modules/tb_progschedule/models` to see the defined Yii models and their relations.


A collection is an array of models

A ModelFinder is a class which fetches collections using AJAX

A `CriteriaBuilder` builds a filter to apply when fetching records (think SQL WHERE)

`relationFilters` tells what related items to fetch (Check the example below) 

Loading models from the database:

```JS
// don't forget to require s2 with require(['storage2'])

var finder = new s2.ModelFinder({cacheTTL: 5});
var builder = new s2.CriteriaBuilder();

var relationFilters ={};
relationFilters.ShowBlockModel = ['/']; // only fetch the block itself
relationFilters.ShowInstanceModel = ['/showDefinition']; //for show instances, also fetch show definition
relationFilters.ShowDefinitionModel = ['/showBlocks']; //... and for show definitions also fetch related show blocks

builder.compare('t.isDeleted', 0, '='); //filter non-deleted

var finderConfig = {
modelName: 'ShowInstanceModel', // we need ShowInstance models
relationFilters: relationFilters,  //with their related records as specified
criteria: builder.toJS() //filtered by these parameters
};
                
var promise = finder.getCollection(finderConfig) //the promise resolves to an  array of ShowInstanceModel objects

```


Creating a model, setting/getting related items:

```JS

//create a new TopicModel model
var topic = factory.createModelSync({
                        modelName: 'TopicModel',
                        attributes: {title: 'Foobar'},
                    });

//retrieve related keywords. Useless, since the topic is brand new
var previousKeywords = topic.getRelated("keywords") //array

// get 2 keyword model instances; 
// getCachedModel ensures the instances are unique throughout the app, i.e. no duplicates
var kw1 = factory.getCachedModel({modelName: 'KeywordModel', key: 'oranges'});
var kw2 = factory.getCachedModel({modelName: 'KeywordModel', key: 'apples'});

var topic = factory.createModelSync({
                        modelName: 'TopicModel',
                        attributes: {title: 'Foobar'},
                    });

//attach keywords to topic model
Topic.setRelated("keywords", [kw1, kw2])

```

Saving previous `topic` model:

```JS

p.saveModel({
    model:topic, //what to save
    paths:['/', '/keywords'], //and what related models, '/' means self
}).then(function(){    
    //save success, topic now has a primary key
    console.log(topic.getKeyValue())
})

```


Here's how the OnAir page handles topic changes with live notifications:

~~~JS
var MQ = new s2.MessageQueue();
    var eventNames = ['EV_TOPICS_REASSIGNED', 'EV_TOPICS_REORDERED', 'EV_RECORDS_DELETED'];
    function _subscribeToLiveSync(showInstance) {

        MQ.unsubscribe(eventNames, 'ON_AIR_PAGE'); //make sure we don't subscribe twice
        

        MQ.subscribe(
        eventNames, //what events to listen on
        'ON_AIR_PAGE', //an unique ID for this subscription
        
        function (responseBody, data, evName) {
        
            //skip notifications caused by this browser
            if (MQ.isSelfOriginated(responseBody)) {
                return;
            }

            //console debug
            console.log('SHOWPLAN: GOT REASSIGN/REORDER/DELETE: ', evName, data);

            // custom logic here
            // for example, sort a model array by title, 
            // maybe another user changed the title for a model we are displaying
            

        });

    }
    
    //subscribe
    _subscribeToLiveSync(showInstance)

~~~


**Advantages**

 - fetches one or more collections in a single AJAX call (HTTP request)
 - fetches related records using a relationsFilter configuration
 - provides **live notifications** (with websockets) on model create, update, delete
 - has a Model factory, so you are guaranteed not to duplicate models
 - has the ability to save one or more models and their related models (a model tree) in a single AJAX call
 - uses promises, so you can chain, catch/rethrow errors, etc. 

**Disadvantages**

 - lacking documentation


## 4.2. Data flow


Developing with Javascript:

 - you use s2 (strongly recommended) or kopi (or ,if you want to make your life miserable, both ) to fetch and render data
 - if using s2,you can also subscribe to live notifications so you can update the UI/DOM when other users change the database models
 

**Special mentions**

The s2 backend is also written in Yii, and is located at `teambutler/protected/modules/tb_progschedule`.

The API controller is `teambutler/protected/modules/tb_progschedule/controllers/ApiController.php`

When Yii modifies a record (create, update, delete), PHP tells the websocket server (2.b) to notify connected browsers.
This is how live notifications are implemented

### 4.3 Topic editor: collaborative editing

This is a special situation. 
When editing a topic, teambutler loads it into an iframe as an etherpad-lite (2.c) document
Whenever changes occur, the s2 model of the topic is updated as well


## 5. Vagrant 

[Vagrant](https://www.vagrantup.com/)  is a software utility designed to ease the process of sharing a  development environment
For developing teambutler you will be using a vagrant box, which is basically a virtual machine running Ubuntu Linux

The Teambutler Vagrant box has a preconfigured environment, including nginx, mySQL, node,js, PHP and all the other requireed software.

### Starting/stopping Vagrant

Start:

```BASH
vagrant up
vagrant ssh
tb_info #this will list URLs and passwords for build, development and PhpMyAdmin
tb_live
```

Stop (make sure you are in the host terminal, not in vagrant ssh terminal):

```BASH
vagrant halt
```

### Loading uncompressed JS files for development:

<http://11.11.11.11:81>

The password for this url is stored in project's `/TeamButler.kdbx`,
the key file is `TeamButler.key`

Password "name" is "Developer access on port 81"
 
You need the free [KeePass](https://keepass.info/) software to use it.

### Loading the build

`./build.sh` first, then go to
 
<http://11.11.11.11> 

### PHPMyAdmin

<http://11.11.11.11:88/>

User is `root`, the pass is in the KeePass db mentioned above, password "name" is "MySQL tunnel"

## 6. FAQ

 NOTE: This is where your [issues](https://bitbucket.org/tudorilisoi/tb-doc/issues?status=new&status=open) will be answered  

Please ask your questions!
